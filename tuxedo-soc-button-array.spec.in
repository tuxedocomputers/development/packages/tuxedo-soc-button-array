Name:           tuxedo-soc-button-array
Version:        #MODULE_VERSION#
Release:        1%{?dist}
Summary:        Temporary downstream fix for the soc_button_array driver

License:        GPLv2
Url:            https://www.tuxedocomputers.com
Source0:        %{name}-%{version}.tar.xz

Requires:       dkms >= 2.1

BuildArch:      noarch

Group:          Hardware/Other
Packager:       TUXEDO Computers GmbH <tux@tuxedocomputers.com>

%description
As of 15.12.2023 the soc_button_array driver in the upstream Linux kernel is
missing an implementation of a flight-mode switch. This package provides a
patched version to install via DKMS to bridge the gap till the implementation
reaches the distributions kernels.

%prep
%setup -q

%install
cp -r %{_builddir}/%{name}-%{version}/usr %{buildroot}

%files
%{_usrsrc}/%{name}-%{version}
%license LICENSE

%post
# Install modules via DKMS
dkms add -m %{name} -v %{version} --rpm_safe_upgrade
dkms build -m %{name} -v %{version}
dkms install -m %{name} -v %{version}
# Attempt to (re-)load module, fail silently if not possible
echo "(Re)load module if possible"
rmmod soc_button_array > /dev/null 2>&1 || true
modprobe soc_button_array > /dev/null 2>&1 || true

%preun
# Remove modules via DKMS
dkms remove -m %{name} -v %{version} --all --rpm_safe_upgrade
# Attempt to (re-)load module, fail silently if not possible
echo "(Re)load module if possible"
rmmod soc_button_array > /dev/null 2>&1 || true
modprobe soc_button_array > /dev/null 2>&1 || true

%changelog
* Wed Jan 10 2024 Werner Sembach <tux@tuxedocomputers.com> 1.0.1-1
- Fix source url
* Fri Dec 15 2023 Werner Sembach <tux@tuxedocomputers.com> 1.0.0-1
- Initial release
